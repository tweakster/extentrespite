import React from 'react';
import ReactDOM from 'react-dom';
import Respite from './Respite';

ReactDOM.render(<Respite />, document.getElementById('root'));
