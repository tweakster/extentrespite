import React from 'react';

class SpaceHdr extends React.Component {
  constructor(props) {
    super(props);

    this.lang = {
      headers: {
        name: null,
        status: 'Status',
        address: 'Address',
        hours: 'Open Hours',
      }
    };
  }

  render() {
    return (
      <div className="row">
        <div className={"d-none d-md-block col-md-3 my-3"}>
         { this.lang.headers.name }
        </div>
        <div className="d-none d-md-block col-md-2 my-3">
         { this.lang.headers.status }
        </div>
        <div className="d-none d-md-block col-md-4 my-3">
         { this.lang.headers.address }
        </div>
        <div className="d-none d-md-block col-md-3 my-3">
         { this.lang.headers.hours }
        </div>
      </div>
    );
  }
}

class Space extends React.Component {
  constructor(props) {
    super(props);

    this.lang = {
      state: {
        spaces: "Spaces",
        busy: "Busy",
        full: "Full",
      }
    };
  }

  format_time(time) {
    try {
      const parts = time.split(':');
      return parts.slice(0,2).join(':');
    }
    catch(err) {
      return null;
    }
  }

  render() {
    let stateClass;
    let stateTitle;

    switch (this.props.data.status) {
      case 0:
        stateClass = "alert-success";
        stateTitle= this.lang.state.spaces;
        break;
      case 1:
        stateClass = "alert-warning";
        stateTitle= this.lang.state.busy;
        break;
      case 2:
        stateClass = "alert-danger";
        stateTitle= this.lang.state.full;
        break;
    }

    let hours;
    if (this.props.data.opens === null) {
      hours = '24 hours';
    } else {
      const start = this.format_time(this.props.data.opens);
      const end = this.format_time(this.props.data.closes);
      hours = `${start} - ${end}`;
    }
    
    return (
      <div className="row">
        <div className="col-sm-12 col-md-3 my-2">
            <h3>{this.props.data.name}</h3>
        </div>
        <div className="col-sm-3 col-md-2 my-2">
          <div className={"alert " + stateClass} role="alert">
            {stateTitle}
          </div>
        </div>
        <div className="col-xs-12 col-sm-5 col-md-4 my-2">
          <ul className="list-unstyled">
          <li>{this.props.data.addr1}</li>
          <li>{this.props.data.addr2}</li>
          <li>{this.props.data.postcode}</li>
          </ul>
        </div>
        <div className="col-xs-12 col-sm-4 col-md-3 my-2">
          {hours}
        </div>
      </div>
    );
  }
}

class Respite extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
      loadError: null,
      spaces: []
    };
  }

  componentDidMount() {
    fetch("/api/spaces")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            spaces: result.spaces
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    return (
      <div className="Respute">
        <header className="navbar justify-content-center" style={{"backgroundColor": "#21a73d"}}>
          <h2>October Rebellion - Accomodation</h2>
        </header>
        <div className="container">
          <SpaceHdr />
          {this.state.spaces.map(spc =>
            <Space key={spc.id} data={spc}/>
          )}
        </div>
      </div>
    );
  }
}

export default Respite;
